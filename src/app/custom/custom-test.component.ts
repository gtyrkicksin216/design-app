import { Component } from '@angular/core';

@Component({
  selector: 'custom-test',
  templateUrl: './custom-test.component.html',
  styleUrls: [ './custom-test.component.scss' ],
})

export class CustomTestComponent { }

/**
 * For this one the directive tied to the component does not need to be referenced here
 * since it is imported into the app module, and the directive uses Renderer2 and
 * ElementRef to grab the event target.
 */
