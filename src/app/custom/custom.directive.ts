import { Directive, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[customDirective]',
})

export class CustomDirective {
  /**
   * Declare your properties here
   */
  uniInputFocus: Function;
  uniInputBlur: Function;
  uniInputEl: any;
  uniInputParent: any;
  testFunc: Function;

  /*
    Here you will use a constructor to set up your Renderer and ElementRef properties
    You will then set up your function with the Renderer2 built in listen method
    and pass an event into the function. From there you can use that event to find
    the Parent element and do something with it.
  */
  constructor(private renderer: Renderer2, private el: ElementRef) {
    this.uniInputFocus = renderer.listen(el.nativeElement, 'focus', (e) => {
      this.uniInputEl = <HTMLInputElement>e.target;
      this.uniInputParent = <HTMLElement>this.uniInputEl.parentElement;
      this.uniInputParent.classList.add(`uni-input--focused`);
    });

    this.uniInputBlur = renderer.listen(el.nativeElement, 'blur', (e) => {
      if (this.uniInputEl.value.length === 0) {
        this.uniInputParent.classList.remove(`uni-input--focused`);
        this.uniInputParent.classList.remove(`uni-input--filled`);
      } else if (this.uniInputEl.value.length !== 0) {
        this.uniInputParent.classList.remove(`uni-input--focused`);
        this.uniInputParent.classList.add(`uni-input--filled`);
      }
    });
  }
}
