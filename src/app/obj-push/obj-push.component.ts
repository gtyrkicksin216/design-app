import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'obj-push',
  templateUrl: './obj-push.component.html',
  styleUrls: [ './obj-push.component.scss' ],
})

export class ObjPushComponent {

  formInfo: Array<any> = [];

  nameModel: string;
  ageModel: string;
  favFoodModel: string;

  constructor() {

    this.nameModel = '';
    this.ageModel = '';
    this.favFoodModel = '';

  }

  formInstance = <FormType>{};

  onSubmit(): void {
    const newInstance: FormType = {
      name: this.formInstance.name,
      age: this.formInstance.age,
      favFood: this.formInstance.favFood,
    };
    console.log(newInstance);
    this.formInfo.push(newInstance);
    this.nameModel = this.favFoodModel = this.ageModel = '';
    this.formInstance.name = this.formInstance.favFood = this.formInstance.age = '';
    console.log(`${this.nameModel}  ${this.ageModel}  ${this.favFoodModel}`);
  }
}

interface FormType {
  name: string;
  age: string;
  favFood: string;
}

