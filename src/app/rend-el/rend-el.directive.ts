/*
https://alligator.io/angular/using-renderer2/

https://www.concretepage.com/angular-2/angular-4-renderer2-example#listen

https://stackoverflow.com/questions/35080387/dynamically-add-event-listener-in-angular-2
*/

import { Directive, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: '[rendElRef]',
})

export class RendElDirective {
  focusFunc: Function;
  blurFunc: Function;

  inputValueGrabber: Function;
  inputValue: string;

  constructor(private renderer: Renderer2, private el: ElementRef) {


    this.focusFunc = renderer.listen(el.nativeElement, 'focus', (e) => {
      console.log(e);
      console.log(e.target);
      console.log(this.el.nativeElement);
      e.target.value === 'undefined' ? console.error(e.target.value) : console.warn(e.target.value);
    });

    this.blurFunc = renderer.listen(el.nativeElement, 'blur', (e) => {
      this.inputValue = e.target.value;
      console.log(this.inputValue);
      if (this.inputValue.length > 0) {
        this.renderer.setStyle(
          this.el.nativeElement,
          'background-color',
          'black',
        );
      } else if (this.inputValue.length === 0) {
        this.renderer.setStyle(
          this.el.nativeElement,
          'background-color',
          'orange',
        );
      }
    });

    this.inputValueGrabber = renderer.listen(el.nativeElement, 'keydown', (e) => {
      this.inputValue = e.target.value;
      console.log(this.inputValue);
    });

  }

}

@Directive({
  selector: '[otherDirective]',
})

export class OtherDirective {

  testDirective: Function;

  constructor(private renderer: Renderer2, private el: ElementRef) {

    this.testDirective = renderer.listen(el.nativeElement, 'click', () => {
      console.log(`testing stacked directives`);
    });

  }

}
