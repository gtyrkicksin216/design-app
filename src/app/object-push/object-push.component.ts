import { Component } from '@angular/core';
import { ReactiveFormsModule, FormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'object-push',
  templateUrl: './object-push.component.html',
  styleUrls: [ './object-push.component.scss' ],
})

export class ObjectPushComponent {
  form: FormGroup;
  formTable: Array<FormType> = [];
  constructor(private _fb: FormBuilder) {
    this.resetForm();
  }
  onSubmit() {
    this.formTable.push(this.form.value);
    this.resetForm();
  }
  private resetForm() {
    this.form = this._fb.group({
      field1: ['', Validators.required],
      field2: ['', Validators.required],
      field3: ['', Validators.required],
    });
  }
}

// This is what I was saying you probably needed but could not remember how exactly to do it.
// You can declare these elsewhere and import them too to keep your component clean;
interface FormType {
  field1: string;
  field2: string;
  field3: string;
}
