import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
// ***Import your directive to use here
import { InputDirective } from './input.directive';
// ***This also needs to be imported to the app-module to allow access to methods

@Component({
  selector: 'input-test',
  templateUrl: './input-test.component.html',
  styleUrls: [ './input-test.component.scss' ],
})

export class InputTestComponent {

  // @Output() inputValue = new EventEmitter;

  // ***Here you declare the @ViewChild property to use to access the directives methods
  @ViewChild(InputDirective) vc: InputDirective;

  // inputFocus: boolean = this.vc.inputFocus;
  // inputFilled: boolean = this.vc.inputFilled;

  // ***This method is run on focus (from template) which accesses the method on the directive
  // ***and passes the event to allow logic to be used inside of the directive
  onFocus($event): void {
    this.vc.onFocus($event);
    // console.log(this.inputFocus);
    // console.log(this.inputFilled);
  }

  // ***Same here but just on blur
  onBlur($event): void {
    this.vc.onBlur($event);
    // console.log(this.inputFocus);
    // console.log(this.inputFilled);
  }

}
