import { Directive, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appInput]',
})

export class InputDirective {

  // ***Here you set up the directives properties
  uniInput: string;
  inputEl: HTMLInputElement;
  inputParent: HTMLElement;
  inputValue: string;

  // inputFocus = false;
  // inputFilled = false;

  constructor() { }

  // ***Set up directive methods here, which can be accessed from the 'parent' component
  onFocus($event) {
    this.inputEl = $event.target;
    this.inputParent = this.inputEl.parentElement;
    this.inputValue = this.inputEl.value;
    this.inputParent.classList.add(`uni-input--focused`);
    // this.inputFocus = !this.inputFocus;
  }

  onBlur($event) {
    this.inputValue = this.inputEl.value;
    if (this.inputValue.length === 0) {
      this.inputParent.classList.remove(`uni-input--focused`);
      this.inputParent.classList.remove(`uni-input--filled`);
      // this.inputFocus = !this.inputFocus;
    } else if (this.inputValue.length !== 0) {
      this.inputParent.classList.remove(`uni-input--focused`);
      this.inputParent.classList.add(`uni-input--filled`);
      // this.inputFocus = !this.inputFocus;
      // this.inputFilled = !this.inputFilled;
    }
  }

}
