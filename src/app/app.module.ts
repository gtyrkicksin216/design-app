import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { TestOneComponent } from './test-one/test-one.component';

import { TestOneDirective } from './test-one/test-one.directive';
import { RendElDirective, OtherDirective } from './rend-el/rend-el.directive';
import { CustomDirective } from './custom/custom.directive';
import { ListItemComponent } from './test-one/list-item/list-item.component';
import { ClassToggleComponent } from './class-toggle/class-toggle.component';
import { RendElComponent } from './rend-el/rend-el.component';
import { CustomTestComponent } from './custom/custom-test.component';
import { InputTestComponent } from './input-test/input-test.component';

// import { ObjectPushComponent } from './object-push/object-push.component';
import { ObjPushComponent } from './obj-push/obj-push.component';

import { InputDirective } from './input-test/input.directive';


@NgModule({
  declarations: [
    AppComponent,
    TestOneDirective,
    RendElDirective,
    OtherDirective,
    CustomDirective,
    InputDirective,
    TestOneComponent,
    ListItemComponent,
    ClassToggleComponent,
    RendElComponent,
    CustomTestComponent,
    InputTestComponent,
    // ObjectPushComponent,
    ObjPushComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
