import { Component } from '@angular/core';

@Component({
  selector: 'class-toggle',
  templateUrl: './class-toggle.component.html',
  styleUrls: [ './class-toggle.component.scss' ],
})

export class ClassToggleComponent {
  items = [
    'CT list item one',
    'CT list item two',
    'CT list item three',
  ];

  index: number;

  // The initial value for show content is set here;
  // showContent = false;
  // An undefined boolean variable in javascript is true
  showContent: boolean;

  showContentTwo: Array<boolean> = [];

  // show(index: number) {
  //   console.log(this[index]);
  //   console.warn(index);
  //   console.log(this.showContentTwo);
  // }

}

