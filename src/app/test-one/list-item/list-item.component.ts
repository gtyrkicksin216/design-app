import { Component } from '@angular/core';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: [ './list-item.component.scss' ],
})

export class ListItemComponent {

  items = [
    'List item One',
    'List item Two',
    'List item Three',
  ];

}

