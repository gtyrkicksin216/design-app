import { Directive, ElementRef, HostListener } from '@angular/core';

/*
https://angular.io/guide/displaying-data

https://angular.io/guide/attribute-directives

https://alligator.io/angular/viewchild-access-component/

https://netbasal.com/event-emitters-in-angular-13e84ee8d28c

===========================================================================

https://alligator.io/angular/using-renderer2/

https://www.concretepage.com/angular-2/angular-4-renderer2-example#listen


*/

@Directive({
  /*
    You would add this attribute selector to every element that
    you would like to have access to these methods
    The one downfall to this way is that when you attach this directive
    it would apply all of these methods
  */
  selector: '[testOne]',
})

export class TestOneDirective {

  /*
    In the constructor you set up your ElementRef property
  */
  constructor(private el: ElementRef) { }

  /*
    With @HostListener you can use listeners like you're used to in javascript
  */
  @HostListener('mouseenter') onMouseEnter() {
    this.bgColor('lightgrey');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.bgColor('initial');
  }

  @HostListener('click') onclick() {
    this.clickerThing(`test`);
  }


  /*
    Here is where you would decalre your methods and properties to use inside of your host listener functions a
    `?` after your property name means that it is not required to be used or is optional when calling that method
  */
  private bgColor(bgColor?: string, classToRemove: string = 'test') {
    this.el.nativeElement.style.backgroundColor = bgColor;
    this.el.nativeElement.classList.remove(classToRemove);
  }

  private clickerThing(classToAdd: string) {
    this.el.nativeElement.classList.add(classToAdd);
    setTimeout(() => {
      this.el.nativeElement.classList.remove(classToAdd);
    }, 10000);
  }

}

